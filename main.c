#ifndef F_CPU
#define F_CPU 16000000UL // or whatever may be your frequency
#endif
 
#include <avr/io.h>
#include <util/delay.h>                // for _delay_ms()
#include <avr/interrupt.h>
// vPPS out = PC6 / OC3A / OC4A
// Led out = PD5
// 1PPS in = PC7/ICP3/CLK0, PD4/ICP1
// Vi anvender i forste omgang timer 3, dvs. ICP3 som input og OC3A som output

// Clk kan vaere 16Mhz (62.5ns), 2MHz (500ns), 250kHz (4us), 62.5kHz (16us) eller 15.625kHz (64us) afhaengigt af prescaler
// Maximal cnt er 65535
// maximal periode for wrap around er 1/16MHz(4.096ms); 8/2MHz(32.768ms); 64/250kHz (262.144ms); 256/62.5kHz (1.048s); 1024/15.625kHz (4.19s)

void initTimer3(void)
{
	TCCR3A = 1<<6; // Set on compare match
	TCCR3B = 1<<6 | 0b011; //Trigger on rising edge, clk at 62.5kHz
	TIMSK3 = 1<<5 | 1<<1; // Enable interrupt on input compare and on output compare
}

ISR(TIMER3_CAPT_vect) //Timer 3 input compare irq
{
        TCCR3A = 0b10<<6;
		OCR3A = ICR3 + 126*4;
}

ISR(TIMER3_COMPA_vect) //Timer 3 output compare IRQ
{
	//Time to add depends on if rising or falling edge pulse was issued last
	if(PINC & 1<<6)
    {
        TCCR3A = 0b10<<6;
		OCR3A = OCR3A + 126*4;
    }
    else
    {
        TCCR3A = 0b01<<6;
        OCR3A = OCR3A + 6124*4;
    }
}
 
int main(void)
{
    DDRD |= 1<<5;                       // initialize port D
    initTimer3();
    DDRC |= 1<<6;
    sei();
    while(1)
    {
        // LED off
        PORTD |= 1<<5;            // PD5 = High = Vcc
        _delay_ms(500);                // wait 500 milliseconds
 
        //LED on
        PORTD &= ~(1<<5);            // PD5 = Low = 0v
        _delay_ms(500);                // wait 500 milliseconds
    }
}

